Description: >
  Server service for the Myapp with DNS setup

Parameters:
  VPC:
    Description: The VPC that the ECS cluster is deployed to
    Type: AWS::EC2::VPC::Id

  Cluster:
    Description: Please provide the ECS Cluster ID that this service should run on
    Type: String

  DesiredCount:
    Description: How many instances of this task should we run across our cluster?
    Type: Number
    Default: 2

  MaxCount:
    Description: Maximum number of instances of this task we can run across our cluster
    Type: Number
    Default: 3

  Listener:
    Description: The Application Load Balancer listener to register with
    Type: String

  LoadBalancerUrl:
    Description: The LoadBalancer URL for CNAME
    Type: String

  MyappServerUrl:
    Description: The URL of the Myapp Server to be passes as an env var in the Docker Container
    Type: String

  Host:
    Description: The host name that the Listener should look for.
    Type: String

  ECSServiceAutoScalingRoleARN:
    Description: The ECS service auto scaling role ARN
    Type: String

Resources:
  Service:
    Type: AWS::ECS::Service
    DependsOn: ListenerRule
    Properties:
      Cluster: !Ref Cluster
      Role: !Ref ServiceRole
      DesiredCount: !Ref DesiredCount
      TaskDefinition: !Ref TaskDefinition
      LoadBalancers:
        - ContainerName: "myapp-client"
          ContainerPort: 3000
          TargetGroupArn: !Ref TargetGroup

  TaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: myapp-client
      ContainerDefinitions:
        - Name: myapp-client
          Essential: true
          Image: [myapp-image-id].dkr.ecr.us-east-1.amazonaws.com/myapp-client:latest
          Memory: 350
          Environment:
            - Name: PORT
              Value: 3000
            - Name: SERVER_URL
              Value: !Ref MyappServerUrl
          PortMappings:
            - ContainerPort: 3000
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref CloudWatchLogsGroup
              awslogs-region: !Ref AWS::Region

  CloudWatchLogsGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Ref AWS::StackName
      RetentionInDays: 365

  TargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      VpcId: !Ref VPC
      Port: 80
      Protocol: HTTP
      Matcher:
        HttpCode: 200-499
      HealthCheckIntervalSeconds: 10
      HealthCheckPath: /
      HealthCheckProtocol: HTTP
      HealthCheckTimeoutSeconds: 5
      HealthyThresholdCount: 2

  ListenerRule:
    Type: AWS::ElasticLoadBalancingV2::ListenerRule
    Properties:
      ListenerArn: !Ref Listener
      Priority: 2
      Conditions:
        - Field: host-header
          Values:
            - !Ref Host
      Actions:
        - TargetGroupArn: !Ref TargetGroup
          Type: forward

  ClientDnsRecordSet:
    Type: AWS::Route53::RecordSet
    Properties:
      HostedZoneName: myappzone.com.
      Name: !Ref Host
      Type: CNAME
      TTL: "900"
      ResourceRecords:
        - !Ref LoadBalancerUrl

  # This IAM Role grants the service access to register/unregister with the
  # Application Load Balancer (ALB). It is based on the default documented here:
  # http://docs.aws.amazon.com/AmazonECS/latest/developerguide/service_IAM_role.html
  ServiceRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Sub ecs-service-${AWS::StackName}
      Path: /
      AssumeRolePolicyDocument: |
        {
            "Statement": [{
                "Effect": "Allow",
                "Principal": { "Service": [ "ecs.amazonaws.com" ]},
                "Action": [ "sts:AssumeRole" ]
            }]
        }
      Policies:
        - PolicyName: !Sub ecs-service-${AWS::StackName}
          PolicyDocument:
            {
              "Version": "2012-10-17",
              "Statement":
                [
                  {
                    "Effect": "Allow",
                    "Action":
                      [
                        "ec2:AuthorizeSecurityGroupIngress",
                        "ec2:Describe*",
                        "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
                        "elasticloadbalancing:Describe*",
                        "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
                        "elasticloadbalancing:DeregisterTargets",
                        "elasticloadbalancing:DescribeTargetGroups",
                        "elasticloadbalancing:DescribeTargetHealth",
                        "elasticloadbalancing:RegisterTargets",
                      ],
                    "Resource": "*",
                  },
                ],
            }

  ServiceScalableTarget:
    Type: "AWS::ApplicationAutoScaling::ScalableTarget"
    Properties:
      MaxCapacity: !Ref MaxCount
      MinCapacity: !Ref DesiredCount
      ResourceId: !Join
        - /
        - - service
          - !Ref Cluster
          - !GetAtt Service.Name
      RoleARN: !Ref ECSServiceAutoScalingRoleARN
      ScalableDimension: ecs:service:DesiredCount
      ServiceNamespace: ecs

  ServiceScaleOutPolicy:
    Type: "AWS::ApplicationAutoScaling::ScalingPolicy"
    Properties:
      PolicyName: ServiceScaleOutPolicy
      PolicyType: StepScaling
      ScalingTargetId: !Ref ServiceScalableTarget
      StepScalingPolicyConfiguration:
        AdjustmentType: ChangeInCapacity
        Cooldown: 1800
        MetricAggregationType: Average
        StepAdjustments:
          - MetricIntervalLowerBound: 0
            ScalingAdjustment: 1

  ServiceScaleInPolicy:
    Type: "AWS::ApplicationAutoScaling::ScalingPolicy"
    Properties:
      PolicyName: ServiceScaleInPolicy
      PolicyType: StepScaling
      ScalingTargetId: !Ref ServiceScalableTarget
      StepScalingPolicyConfiguration:
        AdjustmentType: ChangeInCapacity
        Cooldown: 1800
        MetricAggregationType: Average
        StepAdjustments:
          - MetricIntervalUpperBound: 0
            ScalingAdjustment: -1
