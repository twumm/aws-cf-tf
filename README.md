# CloudFormation Templates for Myapp Finance

#### Prerequisites

1. An S3 bucket
2. Repositories in ECR
3. IAM creds for GitLab CI/CD with permissions to push to ECR, S3 write
   and CloudFormation update stack

#### Things to Change in templates

1. Bucket URLs in the `master.yaml` file
2. Instance Type (current: t2.micro)
3. Image URL for the container in the services

#### Steps

1. Create a bucket with a name of your choice
2. Change the nested template bucket URLs in all the files and upload
3. After uploading, go to CloudFormation console and create a stack with S3 URL pointing to `master.yaml`
4. After a few minutes, you should have the whole stack setup and ready to go. Check the output tab of the stack to find the URLs
5. You can setup any CNAME for the ALB through Route53 or your domain manager.

This template design is inspired from the [official AWS template](https://github.com/aws-samples/ecs-refarch-cloudformation) on GitHub.
Refer to it for more information and any further customization.
